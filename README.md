# DisableGoogleAnalytics
Attempts To Disable Common Google Analytics And Ads Receivers In All Affected Packages

### What it does/How it works: ###

On 1st run after rebooting after install the module is assuming you want it to disable the listed 
analytics and ad receivers, so it will:

- Wait till boot is completed
- Then sleep for 2 minutes
- Then test that cpu usage is under 30% before running
- It will then disable the analytics and ad receivers as listed below, and log its progress to /sdcard/fixgoogleanalytics.log

   Common Analytics And Ad Recievers:

   *com.google.android.gms.analytics.AnalyticsJobService*
   *com.google.android.gms.analytics.CampaignTrackingService*
   *com.google.android.gms.measurement.AppMeasurementService*
   *com.google.android.gms.measurement.AppMeasurementJobService*
   *com.google.android.gms.analytics.AnalyticsReceiver*
   *com.google.android.gms.analytics.CampaignTrackingReceiver*
   *com.google.android.gms.measurement.AppMeasurementInstallReferrerReceiver*
   *com.google.android.gms.measurement.AppMeasurementReceiver*
   *com.google.android.gms.measurement.AppMeasurementContentProvider*
   *com.crashlytics.android.CrashlyticsInitProvider*
   *com.google.android.gms.ads.AdActivity*
   *com.google.firebase.iid.FirebaseInstanceIdService*


- Once complete the service.sh script will delete itself
- Once that happens any further interaction is purely via the included **dga** script as a binary
in /system/(x)bin
 
   **dga** takes 2 arguments, **disable** or **enable**, though i doubt too many will be looking to enable 
   analytics and ad receivers, unless the disabling has unwanted effects on their system, which
   is high time to include the usual disclaimer that you run this at your own risk and not even
   dga enable is garaunteed to undo the changes

---

### Module Installation: ###

- Download from **[Releases](https://gitlab.com/adrian.m.miller/disablegoogleanalytics/-/releases)**  
![](https://gitlab.com/adrian.m.miller/disablegoogleanalytics/-/badges/release.svg)
- Install the module via Magisk app/Fox Magisk Module Manager/MRepo
- Reboot

---

### Usage: ###

- After first run optmization has completed, any further interaction is purely via the included **dga** script as a binary
in /system/(x)bin:

**dga** takes 2 arguments, **disable** or **enable**, though i doubt too many will be looking to enable 
analytics and ad receivers, unless the disabling has unwanted effects on their system, which
is high time to include the usual disclaimer that you run this at your own risk and not even
dga enable is garaunteed to undo the changes

**Uninstall Note:** Uninstalling the module **will not reverse the changes**, if your intention is to uninstall the module and re-enable the
analytics and ad receivers, please run **dga enable** first

---

### Changelog: ###

Please see: https://gitlab.com/adrian.m.miller/disablegoogleanalytics/-/blob/main/changelog.md

---


